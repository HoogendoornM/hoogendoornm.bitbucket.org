class GameObject {
    constructor() {
        this._health = 10;
        this.active = true;
        this.isDestroyed = false;
        this.radius = 10;
        this.lineWidth = 2;
        this.color = "red";
    }
    get x() {
        return this._x;
    }
    set x(value) {
        this._x = value;
    }
    get y() {
        return this._y;
    }
    set y(value) {
        this._y = value;
    }
    get width() {
        return this._width;
    }
    set width(value) {
        this._width = value;
    }
    get height() {
        return this._height;
    }
    set height(value) {
        this._height = value;
    }
    get health() {
        return this._health;
    }
    set health(value) {
        this._health = value;
    }
    get type() {
        return this._type;
    }
    set type(value) {
        this._type = value;
    }
}
class Asteroid extends GameObject {
    constructor(x, y, width, height, health, src) {
        super();
        this.img = new Image;
        this.explosionImg = new Image;
        this.explosionAudio = new Audio("./assets/audio/explosion.wav");
        this.render = () => {
            if (this.health <= 0) {
                if (!this.isDestroyed) {
                    this.destroy();
                }
                this.isDestroyed = true;
            }
            if (!this.active) {
                return;
            }
            ctx.save();
            this.x += this.moveX;
            this.y += this.moveY;
            if (!this.isDestroyed) {
                ctx.drawImage(this.img, this.x, this.y, this.width, this.height);
            }
            else {
                ctx.drawImage(this.explosionImg, this.x, this.y, this.width, this.height);
            }
            ctx.restore();
        };
        this.destroy = () => {
            let temp = this;
            if (temp.width > 150) {
                var randomX = (Math.random() * 100) - 50;
                var randomY = (Math.random() * 100) - 50;
                var asteroid1 = new Asteroid((temp.x + temp.width / 4) + randomX, (temp.y + (temp.height / 4)) + randomY, (temp.width / 2), (temp.height / 2), 30, "./assets/images/asteroid2.png");
                var asteroid2 = new Asteroid((temp.x + temp.width / 4) + (randomX * -1), (temp.y + (temp.height / 4)) + (randomY * -1), (temp.width / 2), (temp.height / 2), 30, "./assets/images/asteroid2.png");
                objectArray.push(asteroid1);
                objectArray.push(asteroid2);
            }
            else {
                var randomType;
                var typeRng = Math.floor(Math.random() * 3) + 1;
                console.log(typeRng);
                if (typeRng == 1) {
                    randomType = "gold";
                }
                if (typeRng == 2) {
                    randomType = "silver";
                }
                if (typeRng == 3) {
                    randomType = "copper";
                }
                var pickup1 = new Pickup(temp.x, temp.y, 70, 70, randomType, "./assets/images/" + randomType + ".png");
                objectArray.push(pickup1);
                console.log(randomType + " spawned");
            }
            this.explosionAudio.currentTime = 0;
            this.explosionAudio.play();
            setTimeout(function () {
                temp.active = false;
            }, 150);
        };
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.health = health;
        this.startHealth = health;
        this.img.src = src;
        this.img.width = this.width;
        this.img.height = this.height;
        this.explosionImg.src = "./assets/images/explosion.png";
        this.explosionImg.width = this.width;
        this.explosionImg.height = this.height;
        this.moveX = (Math.random() * 2) - 1;
        this.moveY = (Math.random() * 2) - 1;
    }
}
class Bullet extends GameObject {
    constructor(x, y, size, color = "red", lineWidth = 5) {
        super();
        this._active = true;
        this.size = 10;
        this.speed = 5;
        this._velocity = new Vector2(0, 0);
        this.animateValue = 0;
        this.animateBool = true;
        this.bulletHitAudio = new Audio("./assets/audio/bulletHit.wav");
        this.render = () => {
            if (!this.active) {
                return;
            }
            if (this.animateBool) {
                this.animateValue += 0.1;
                if (this.animateValue >= 2) {
                    this.animateBool = false;
                }
            }
            else {
                this.animateValue -= 0.1;
                if (this.animateValue <= -2) {
                    this.animateBool = true;
                }
            }
            this.x += this.velocity.x;
            this.y += this.velocity.y;
            if (this.x < 0 || this.x > canvas.width || this.y < 0 || this.y > canvas.height) {
                this.active = false;
            }
            ctx.save();
            ctx.beginPath();
            ctx.strokeStyle = this.color;
            ctx.lineWidth = this.lineWidth + this.animateValue;
            ctx.rect(this.x, this.y, this.size, this.size);
            ctx.stroke();
            ctx.restore();
            this.color = "red";
            this.size = 2;
            objectArray.forEach(element => {
                var offset = 4;
                if (element.active == false) {
                    return;
                }
                var offset = element.width / 4;
                if ((element.x + offset) - this.x < 1 && (element.x + element.width - offset) - this.x > -1 &&
                    (element.y + offset) - this.y < 1 && (element.y + element.height - offset) - this.y > -1) {
                    this.velocity = new Vector2(0, 0);
                    element.health--;
                    this.bulletHitAudio.volume = 0.3;
                    this.bulletHitAudio.currentTime = 0;
                    this.bulletHitAudio.play();
                    let temp = this;
                    temp.color = "yellow";
                    temp.size = 5;
                    setTimeout(function () {
                        temp.active = false;
                    }, 100);
                }
            });
        };
        this.move = (orientation) => {
            this.velocity.copy(orientation);
            this.velocity.multiply(this.speed);
        };
        this.x = x;
        this.y = y;
        this.size = size;
        this.color = color;
        this.lineWidth = lineWidth;
    }
    get velocity() {
        return this._velocity;
    }
    set velocity(value) {
        this._velocity = value;
    }
    get active() {
        return this._active;
    }
    set active(value) {
        this._active = value;
    }
}
class Circle extends GameObject {
    constructor(x, y, radius, color, lineWidth) {
        super();
        this.render = () => {
            ctx.save();
            ctx.beginPath();
            ctx.strokeStyle = this.color;
            ctx.lineWidth = this.lineWidth;
            ctx.globalAlpha = 0.5;
            ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
            ctx.closePath();
            ctx.stroke();
            ctx.restore();
        };
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.lineWidth = lineWidth;
    }
}
class Enemy extends GameObject {
    constructor(x, y, width, height, health, src) {
        super();
        this.img = new Image;
        this.explosionImg = new Image;
        this.speed = 1;
        this.rotation = 0;
        this.explosionAudio = new Audio("./assets/audio/explosion.wav");
        this.render = () => {
            if (this.health <= 0) {
                if (!this.isDestroyed) {
                    this.destroy();
                }
                this.isDestroyed = true;
            }
            if (!this.active) {
                return;
            }
            if (this.x + (this.width / 2) < player.x) {
                this.moveX = this.speed;
            }
            else {
                this.moveX = -this.speed;
            }
            if (this.y + (this.height / 2) < player.y) {
                this.moveY = this.speed;
            }
            else {
                this.moveY = -this.speed;
            }
            this.x += this.moveX;
            this.y += this.moveY;
            ctx.save();
            if (!this.isDestroyed) {
                ctx.drawImage(this.img, this.x, this.y, this.width, this.height);
            }
            else {
                ctx.drawImage(this.explosionImg, this.x, this.y, this.width, this.height);
            }
            ctx.restore();
        };
        this.destroy = () => {
            let temp = this;
            player.activeEnemies--;
            var healthPickup = new Pickup((temp.x + temp.width / 2), (temp.y + temp.height / 2), 70, 70, "health", "./assets/images/heart.png");
            objectArray.push(healthPickup);
            console.log("Health spawned");
            this.explosionAudio.currentTime = 0;
            this.explosionAudio.play();
            setTimeout(function () {
                temp.active = false;
            }, 150);
        };
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.health = health;
        this.startHealth = health;
        this.img.src = src;
        this.img.width = this.width;
        this.img.height = this.height;
        this.explosionImg.src = "./assets/images/explosion.png";
        this.explosionImg.width = this.width;
        this.explosionImg.height = this.height;
    }
}
let canvas;
let ctx;
let objectArray = new Array();
let bulletArray = new Array();
let bgObjectArray = new Array();
let player;
class Game {
    constructor() {
        this.fps = 60;
        this.timer = 0;
        this.waveTimer = 1000;
        this.alarmAudio = new Audio("./assets/audio/alarm.wav");
        this.gameLoop = () => {
            var Loop = this.gameLoop;
            setTimeout(function () {
                requestAnimationFrame(Loop);
            }, 1000 / this.fps);
            this.timer++;
            if (this.timer % this.waveTimer == 0) {
                this.spawnEnemy();
            }
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            for (var bgObject of bgObjectArray) {
                bgObject.x -= (player.velocity.x * 0.5);
                bgObject.y -= (player.velocity.y * 0.5);
                bgObject.render();
            }
            for (var object of objectArray) {
                object.x -= player.velocity.x;
                object.y -= player.velocity.y;
                object.render();
            }
            player.render();
            player.x = canvas.width / 2;
            player.y = canvas.height / 2;
            for (var bullet of bulletArray) {
                bullet.x -= player.velocity.x;
                bullet.y -= player.velocity.y;
                bullet.render();
            }
            this.uI.render();
        };
        this.spawnEnemy = () => {
            var maxEnemies = 3;
            var spawnDistance = 600;
            if (player.activeEnemies >= maxEnemies || player.isDestroyed) {
                return;
            }
            var enemySpawn = Math.floor(Math.random() * 4) + 1;
            var enemyPosX = 0;
            var enemyPosY = 0;
            if (enemySpawn == 1) {
                enemyPosX = spawnDistance;
                enemyPosY = spawnDistance;
            }
            if (enemySpawn == 2) {
                enemyPosX = spawnDistance;
                enemyPosY = spawnDistance * -1;
            }
            if (enemySpawn == 3) {
                enemyPosX = spawnDistance * -1;
                enemyPosY = spawnDistance;
            }
            if (enemySpawn == 4) {
                enemyPosX = spawnDistance * -1;
                enemyPosY = spawnDistance * -1;
            }
            var ufoRng = Math.floor(Math.random() * 2) + 1;
            objectArray.push(new Enemy(enemyPosX, enemyPosY, 400, 400, 100, "./assets/images/ufo" + ufoRng + ".png"));
            player.activeEnemies++;
            this.alarmAudio.play();
            console.log("enemy spawned " + player.activeEnemies.toString() + " " + enemySpawn.toString());
        };
        console.log("Start");
        canvas = document.getElementById("canvas");
        ctx = canvas.getContext("2d");
        ctx.canvas.width = window.innerWidth;
        ctx.canvas.height = window.innerHeight;
        this.uI = new UserInterface();
        player = new Player(canvas.width / 2, canvas.height / 2, 5, "white", 5, 3);
        this.asteroidAmount = 1000;
        for (let index = 0; index < this.asteroidAmount; index++) {
            var randomX = (Math.random() * 20000) - 10000;
            var randomY = (Math.random() * 20000) - 10000;
            var randomSize = (Math.random() * 500) + 40;
            if (randomX >= player.x - 500 && randomX <= player.x + 500 && randomY >= player.y - 500 && randomY <= player.y + 500) {
                randomX = (Math.random() * 1000) - 3000;
                randomY = (Math.random() * 1000) + 3000;
                console.log("Asteroid moved away from spawn");
            }
            objectArray.push(new Asteroid(randomX, randomY, randomSize, randomSize, (randomSize / 5), "./assets/images/asteroid2.png"));
        }
        this.circleAmount = 150;
        for (let index = 0; index < this.circleAmount; index++) {
            var randomX = (Math.random() * 20000) - 10000;
            var randomY = (Math.random() * 20000) - 10000;
            var randomRadius = (Math.random() * 1000) + 1;
            var randomLineWidth = (Math.random() * 500) + 1;
            var randomColor = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';
            bgObjectArray.push(new Circle(randomX, randomY, randomRadius, randomColor, randomLineWidth));
        }
        objectArray.push(new Pickup(player.x, 500, 60, 60, "health", "./assets/images/heart.png"));
        objectArray.push(new Pickup(player.x + 600, 100, 70, 70, "gold", "./assets/images/gold.png"));
        var audioRng = Math.floor(Math.random() * 2) + 1;
        console.log("audiofile: " + audioRng);
        if (audioRng == 1) {
            this.bgAudio = new Audio("./assets/audio/bgAudio.wav");
        }
        else {
            this.bgAudio = new Audio("./assets/audio/bgAudio2.wav");
        }
        this.bgAudio.loop = true;
        this.bgAudio.volume = 0.1;
        this.bgAudio.play();
        this.gameLoop();
    }
}
let init = function () {
    let main = new Main;
};
window.addEventListener('load', init);
class Main {
    constructor() {
        this.game = new Game();
    }
}
class Pickup extends GameObject {
    constructor(x, y, width, height, type, src) {
        super();
        this.img = new Image;
        this.render = () => {
            if (!this.active) {
                return;
            }
            this.x += this.moveX;
            this.y += this.moveY;
            ctx.save();
            ctx.drawImage(this.img, this.x, this.y, this.width, this.height);
            ctx.restore();
        };
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.type = type;
        this.img.src = src;
        this.img.width = this.width;
        this.img.height = this.height;
        this.moveX = (Math.random() * 2) - 1;
        this.moveY = (Math.random() * 2) - 1;
    }
}
class Player extends GameObject {
    constructor(x, y, size, color, lineWidth, health) {
        super();
        this._velocity = new Vector2(0, 0);
        this.orientation = new Vector2(1, 0);
        this._maxSpeed = 4;
        this.acceleration = 0.2;
        this.rotation = 0;
        this.size = 10;
        this.pointList = new Array();
        this.canShoot = true;
        this.leftPressed = false;
        this.rightPressed = false;
        this.upPressed = false;
        this.downPressed = false;
        this.spacePressed = false;
        this._tempVec = new Vector2(0, 0);
        this._score = 0;
        this.maxHealth = 5;
        this.isDead = false;
        this._activeEnemies = 0;
        this.bulletAudio = new Audio("./assets/audio/laser.wav");
        this.collisionAudio = new Audio("./assets/audio/bump.wav");
        this.pickupAudio = new Audio("./assets/audio/pickup.wav");
        this.healthAudio = new Audio("./assets/audio/heart.wav");
        this.render = () => {
            if (!this.active) {
                return;
            }
            if (this.health <= 0) {
                this.velocity = new Vector2(0, 0);
                this.isDestroyed = true;
                this.isDead = true;
                this.destroy();
                console.log("YOU DIED");
            }
            ctx.save();
            ctx.translate(this.x, this.y);
            ctx.rotate(this.rotation);
            ctx.beginPath();
            if (!this.isDestroyed) {
                ctx.strokeStyle = this.color;
                ctx.lineWidth = this.lineWidth;
            }
            else {
                ctx.strokeStyle = "red";
                ctx.lineWidth = this.lineWidth * 2;
            }
            ctx.moveTo(this.pointList[this.pointList.length - 1].x, this.pointList[this.pointList.length - 1].y);
            for (var index = 0; index < this.pointList.length; index++) {
                ctx.lineTo(this.pointList[index].x, this.pointList[index].y);
            }
            ctx.closePath();
            ctx.stroke();
            ctx.restore();
            if (this.upPressed) {
                this.accelerate();
            }
            if (this.downPressed) {
                this.decelerate();
            }
            if (this.leftPressed) {
                this.turnLeft();
            }
            if (this.rightPressed) {
                this.turnRight();
            }
            if (this.spacePressed) {
                this.shoot();
            }
            objectArray.forEach(element => {
                if (!element.active || this.isDead) {
                    return;
                }
                var offset = element.width / 5;
                if ((element.x + offset) - this.x < 1 && (element.x + element.width - offset) - this.x > -1 &&
                    (element.y + offset) - this.y < 1 && (element.y + element.height - offset) - this.y > -1) {
                    if (element.type == "gold" || element.type == "silver" || element.type == "copper") {
                        this.pickUp(element.type);
                        element.active = false;
                        console.log("pickup collision");
                        return;
                    }
                    if (element.type == "health") {
                        element.active = false;
                        this.healthPickup();
                        console.log("health collision");
                        return;
                    }
                    this.velocity = new Vector2((this.velocity.x * -1) + (element.moveX), (this.velocity.y * -1) + (element.moveY));
                    this.health--;
                    if (this.health < 0) {
                        this.health = 0;
                    }
                    console.log(this.health);
                    if (this.velocity.x <= 1) {
                        this.velocity.x = (this.velocity.x * 3) + element.moveX * 2;
                    }
                    if (this.velocity.y <= 1) {
                        this.velocity.y = (this.velocity.y * 3) + element.moveY * 2;
                    }
                    if (this.velocity.magSq() >= this._maxSpeed) {
                        this.velocity.normalize(this._maxSpeed);
                    }
                    this.collisionAudio.currentTime = 0;
                    this.collisionAudio.play();
                }
            });
        };
        this.destroy = () => {
            setTimeout(function () {
                player.velocity = new Vector2(0, 0);
                player.active = false;
            }, 1000);
        };
        this.accelerate = () => {
            if (this.velocity.x == 0 && this.velocity.y == 0) {
                this.velocity.copy(this.orientation);
                this.velocity.multiply(this.acceleration);
            }
            this._tempVec.copy(this.orientation);
            this._tempVec.multiply(this.acceleration);
            this.velocity.add(this._tempVec);
            if (this.velocity.magSq() >= this._maxSpeed) {
                this.velocity.normalize(this._maxSpeed);
            }
        };
        this.decelerate = () => {
            this.velocity.multiply(0.9);
            if (this.velocity.magSq() < 0.5) {
                this.velocity.x = 0;
                this.velocity.y = 0;
            }
        };
        this.turnLeft = () => {
            this.rotation -= 0.1;
            this.rotation %= Math.PI * 2;
            this.orientation.x = 1;
            this.orientation.y = 0;
            this.orientation.rotate(-this.rotation);
        };
        this.turnRight = () => {
            this.rotation += 0.1;
            this.rotation %= Math.PI * 2;
            this.orientation.x = 1;
            this.orientation.y = 0;
            this.orientation.rotate(-this.rotation);
        };
        this.shoot = () => {
            let bullet;
            if (!this.canShoot) {
                return;
            }
            for (var index = 0; index < bulletArray.length; index++) {
                bullet = bulletArray[index];
                if (!bullet.active) {
                    break;
                }
            }
            if (bullet == null || bullet.active) {
                bullet = new Bullet(player.x, player.y, 3);
                bulletArray.push(bullet);
            }
            else {
                bullet.x = player.x;
                bullet.y = player.y;
                bullet.active = true;
            }
            bullet.move(player.orientation);
            this.bulletAudio.currentTime = 0;
            this.bulletAudio.play();
            this.canShoot = false;
            setTimeout(function () {
                player.canShoot = true;
            }, 200);
        };
        this.pickUp = (type) => {
            if (type == "gold") {
                this.addScore(3);
            }
            if (type == "silver") {
                this.addScore(2);
            }
            if (type == "copper") {
                this.addScore(1);
            }
            this.pickupAudio.currentTime = 0;
            this.pickupAudio.play();
        };
        this.healthPickup = () => {
            this.health++;
            if (this.health > this.maxHealth) {
                this.health = this.maxHealth;
            }
            this.healthAudio.currentTime = 0;
            this.healthAudio.play();
        };
        this.addScore = (value) => {
            this._score += value;
        };
        this.x = x;
        this.y = y;
        this.size = size;
        this.color = color;
        this.lineWidth = lineWidth;
        this.health = health;
        this.pointList.push(new Vector2(3 * size, 0));
        this.pointList.push(new Vector2(-2 * size, -2 * size));
        this.pointList.push(new Vector2(-1 * size, 0));
        this.pointList.push(new Vector2(-2 * size, 2 * size));
        document.addEventListener("keydown", this.keyboardDown);
        document.addEventListener("keyup", this.keyboardUp);
    }
    get maxSpeed() {
        return this._maxSpeed;
    }
    set maxSpeed(value) {
        this._maxSpeed = value;
    }
    get velocity() {
        return this._velocity;
    }
    set velocity(value) {
        this._velocity = value;
    }
    get score() {
        return this._score;
    }
    set score(value) {
        this._score = value;
    }
    get activeEnemies() {
        return this._activeEnemies;
    }
    set activeEnemies(value) {
        this._activeEnemies = value;
    }
    keyboardDown(event) {
        if (event.key == "ArrowLeft" || event.key == "a") {
            player.leftPressed = true;
        }
        if (event.key == "ArrowUp" || event.key == "w") {
            player.upPressed = true;
        }
        if (event.key == "ArrowRight" || event.key == "d") {
            player.rightPressed = true;
        }
        if (event.key == "ArrowDown" || event.key == "s") {
            player.downPressed = true;
        }
        if (event.keyCode == 32) {
            player.spacePressed = true;
        }
    }
    keyboardUp(event) {
        if (event.key == "ArrowLeft" || event.key == "a") {
            player.leftPressed = false;
        }
        if (event.key == "ArrowUp" || event.key == "w") {
            player.upPressed = false;
        }
        if (event.key == "ArrowRight" || event.key == "d") {
            player.rightPressed = false;
        }
        if (event.key == "ArrowDown" || event.key == "s") {
            player.downPressed = false;
        }
        if (event.keyCode == 32) {
            player.spacePressed = false;
        }
    }
}
class UserInterface {
    constructor() {
        this.heartImg = new Image;
        this.deadAudio = new Audio("./assets/audio/dead.wav");
        this.playedAudio = false;
        this.render = () => {
            ctx.fillStyle = "white";
            ctx.font = "35px Verdana";
            ctx.fillStyle = "white";
            ctx.fillText("Score: " + player.score.toString(), canvas.width - 200, 50);
            var heartPos = 50;
            for (let index = 0; index < player.health; index++) {
                ctx.drawImage(this.heartImg, heartPos, 10, 50, 50);
                heartPos += 40;
            }
            if (player.health <= 0) {
                if (!this.playedAudio) {
                    this.deadAudio.play();
                    this.playedAudio = true;
                }
                ctx.font = "50px Verdana";
                ctx.fillStyle = "red";
                ctx.fillText("YOU DIED", (canvas.width / 2) - 100, canvas.height / 3);
                ctx.fillStyle = "white";
                ctx.fillText("Final score: " + player.score.toString(), (canvas.width / 2) - 150, canvas.height / 2);
                ctx.fillStyle = "white";
                ctx.font = "35px Verdana";
                ctx.fillText("Press F5 to restart!", (canvas.width / 2) - 150, canvas.height / 1.5);
            }
        };
        this.heartImg.src = "./assets/images/heart.png";
    }
}
class Vector2 {
    constructor(x = 0, y = 0) {
        this.x = 0;
        this.y = 0;
        this.magnitude = () => {
            return Math.sqrt(this.x * this.x + this.y * this.y);
        };
        this.magSq = () => {
            return this.x * this.x + this.y * this.y;
        };
        this.normalize = (magnitude = 1) => {
            let len = Math.sqrt(this.x * this.x + this.y * this.y);
            this.x /= len / magnitude;
            this.y /= len / magnitude;
            return this;
        };
        this.zero = () => {
            this.x = 0;
            this.y = 0;
        };
        this.copy = (point) => {
            this.x = point.x;
            this.y = point.y;
        };
        this.rotate = (radians) => {
            var cos = Math.cos(radians);
            var sin = Math.sin(radians);
            var x = (cos * this.x) + (sin * this.y);
            var y = (cos * this.y) - (sin * this.x);
            this.x = x;
            this.y = y;
        };
        this.getAngle = () => {
            return Math.atan2(this.x, this.y);
        };
        this.multiply = (value) => {
            this.x *= value;
            this.y *= value;
        };
        this.add = (value) => {
            this.x += value.x;
            this.y += value.y;
        };
        this.substract = (value) => {
            this.x -= value.x;
            this.y -= value.y;
        };
        this.x = x;
        this.y = y;
    }
}
//# sourceMappingURL=main.js.map