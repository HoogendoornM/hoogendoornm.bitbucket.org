/// <reference path="gameObject.ts" />
class Bullet extends GameObject {

    private _active: boolean = true;
    private size: number = 10;
    private speed: number = 5;
    private _velocity: Vector2 = new Vector2(0, 0);

    private animateValue: number = 0;
    private animateBool: boolean = true;

    private bulletHitAudio: HTMLAudioElement = new Audio("./assets/audio/bulletHit.wav");

    constructor(x: number, y: number, size: number, color: string = "red", lineWidth: number = 5) {
        super();
        this.x = x;
        this.y = y;
        this.size = size;
        this.color = color;
        this.lineWidth = lineWidth;
    }

    // Render the object
    public render = (): void => {

        // do nothing if not active
        if (!this.active) {
            return;
        }

        // if true increase animateValue
        if (this.animateBool) {
            this.animateValue += 0.1; // increase size

            // set animateBool false if bigger than value
            if (this.animateValue >= 2) {
                this.animateBool = false;
            }
        } else {
            this.animateValue -= 0.1; // reduce size

            // set animateBool true if under value
            if (this.animateValue <= -2) {
                this.animateBool = true;
            }
        }

        this.x += this.velocity.x;
        this.y += this.velocity.y;

        // deactivate bullet if outside canvas view
        if (this.x < 0 || this.x > canvas.width || this.y < 0 || this.y > canvas.height) {
            this.active = false;
        }

        ctx.save();
        ctx.beginPath();

        ctx.strokeStyle = this.color;
        ctx.lineWidth = this.lineWidth + this.animateValue;

        ctx.rect(this.x, this.y, this.size, this.size)

        //ctx.closePath();
        ctx.stroke();
        ctx.restore();

        // set stats before collision
        this.color = "red";
        this.size = 2;

        // collision test
        objectArray.forEach(element => {
            var offset = 4;
            if (element.active == false) {
                return;
            }
            var offset: number = element.width / 4; // offset for collision
            if ((element.x + offset) - this.x < 1 && (element.x + element.width - offset) - this.x > -1 &&
                (element.y + offset) - this.y < 1 && (element.y + element.height - offset) - this.y > -1) {
                this.velocity = new Vector2(0, 0);
                element.health--; // reduce health 

                // play sound on collision
                this.bulletHitAudio.volume = 0.3;
                this.bulletHitAudio.currentTime = 0; // reset audio on new call
                this.bulletHitAudio.play();

                // destroy bullet animation                
                let temp = this;
                temp.color = "yellow";
                temp.size = 5;
                setTimeout(
                    function () {
                        temp.active = false; // set element active false after x ms
                    }, 100);
            }
        });
    }

    public move = (orientation: Vector2): void => {
        this.velocity.copy(orientation);
        this.velocity.multiply(this.speed); // increase velocity
    }

    get velocity(): Vector2 {
        return this._velocity;
    }
    set velocity(value: Vector2) {
        this._velocity = value;
    }

    get active(): boolean {
        return this._active;
    }
    set active(value: boolean) {
        this._active = value;
    }
}