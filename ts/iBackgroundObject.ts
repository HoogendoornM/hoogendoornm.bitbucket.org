interface iBackgroundObject {

    render(): void;
    x: number;
    y: number;
    width: number;
    height: number;     
    active: boolean;
}
