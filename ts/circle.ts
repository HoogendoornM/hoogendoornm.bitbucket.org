/// <reference path="gameObject.ts" />
// background circles
class Circle extends GameObject implements iBackgroundObject {

    constructor(x: number, y: number, radius: number, color: string, lineWidth: number) { super();
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.lineWidth = lineWidth;
    }
    
    public render = (): void => {
        ctx.save();
        ctx.beginPath();

        ctx.strokeStyle = this.color;
        ctx.lineWidth = this.lineWidth;
        ctx.globalAlpha = 0.5; // change opacity
        ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);


        ctx.closePath();
        ctx.stroke();
        ctx.restore();
    }
}

