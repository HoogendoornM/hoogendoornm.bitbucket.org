/// <reference path="gameObject.ts" />
// object the player can pick up
class Pickup extends GameObject implements iObject {

    private img: HTMLImageElement = new Image;

    constructor(x: number, y: number, width: number, height: number, type: string, src: string) {
        super();
        this.x = x;
        this.y = y;
        this.width = width
        this.height = height
        this.type = type;

        this.img.src = src;
        this.img.width = this.width;
        this.img.height = this.height;

        this.moveX = (Math.random() * 2) - 1;
        this.moveY = (Math.random() * 2) - 1;
    }

    // render enemy
    // runs every frame
    public render = (): void => {

        // do nothing if not active
        if (!this.active) {
            return;
        }

        this.x += this.moveX;
        this.y += this.moveY;

        ctx.save();

        ctx.drawImage(this.img, this.x, this.y, this.width, this.height);        

        ctx.restore();
    }    
}