class Player extends GameObject {

    private _velocity: Vector2 = new Vector2(0, 0);
    private orientation: Vector2 = new Vector2(1, 0);
    private _maxSpeed: number = 4;
    private acceleration: number = 0.2;
    private rotation: number = 0;
    private size: number = 10;
    private pointList: Array<Vector2> = new Array<Vector2>();

    private canShoot: boolean = true;

    // pressed key booleans
    private leftPressed: boolean = false;
    private rightPressed: boolean = false;
    private upPressed: boolean = false;
    private downPressed: boolean = false;
    private spacePressed: boolean = false;

    private _tempVec: Vector2 = new Vector2(0, 0);

    private _score: number = 0;
    private maxHealth: number = 5;
    private isDead: boolean = false;
    private _activeEnemies: number = 0;

    // audio files
    private bulletAudio: HTMLAudioElement = new Audio("./assets/audio/laser.wav");
    private collisionAudio: HTMLAudioElement = new Audio("./assets/audio/bump.wav");
    private pickupAudio: HTMLAudioElement = new Audio("./assets/audio/pickup.wav");
    private healthAudio: HTMLAudioElement = new Audio("./assets/audio/heart.wav");    

    // constructor
    constructor(x: number, y: number, size: number, color: string, lineWidth: number, health: number) {
        super();
        this.x = x;
        this.y = y;
        this.size = size;
        this.color = color;
        this.lineWidth = lineWidth;
        this.health = health;

        // drawing the shape of the ship
        this.pointList.push(new Vector2(3 * size, 0));
        this.pointList.push(new Vector2(-2 * size, -2 * size));
        this.pointList.push(new Vector2(-1 * size, 0));
        this.pointList.push(new Vector2(-2 * size, 2 * size));

        // eventListeners for keyboard input
        document.addEventListener("keydown", this.keyboardDown);
        document.addEventListener("keyup", this.keyboardUp);
    }

    // Render the object
    // runs every gameloop
    public render = (): void => {

        // do nothing if not active
        if (!this.active) {
            return;
        }

        // if no more health
        if (this.health <= 0) {
            this.velocity = new Vector2(0, 0); //stop moving
            this.isDestroyed = true;
            this.isDead = true;
            this.destroy();
            console.log("YOU DIED");
        }

        ctx.save();
        ctx.translate(this.x, this.y);
        ctx.rotate(this.rotation);
        ctx.beginPath();

        // change color & lineWidth if destroyed
        if (!this.isDestroyed) {
            ctx.strokeStyle = this.color;
            ctx.lineWidth = this.lineWidth;
        } else {
            ctx.strokeStyle = "red";
            ctx.lineWidth = this.lineWidth * 2;
        }

        ctx.moveTo(this.pointList[this.pointList.length - 1].x, this.pointList[this.pointList.length - 1].y);

        for (var index = 0; index < this.pointList.length; index++) {
            ctx.lineTo(this.pointList[index].x, this.pointList[index].y)
        }

        ctx.closePath();
        ctx.stroke();
        ctx.restore();

        // button press checks
        if (this.upPressed) {
            this.accelerate();
        }
        if (this.downPressed) {
            this.decelerate();
        }
        if (this.leftPressed) {
            this.turnLeft();
        }
        if (this.rightPressed) {
            this.turnRight();
        }
        if (this.spacePressed) {
            this.shoot();
        }

        // collision test for active elements
        objectArray.forEach(element => {
            if (!element.active || this.isDead) {
                return;
            }

            // test collision
            var offset: number = element.width / 5; // offset for collision
            if ((element.x + offset) - this.x < 1 && (element.x + element.width - offset) - this.x > -1 &&
                (element.y + offset) - this.y < 1 && (element.y + element.height - offset) - this.y > -1) {

                // if collision with pickup
                if (element.type == "gold" || element.type == "silver" || element.type == "copper") {
                    this.pickUp(element.type);
                    element.active = false;
                    console.log("pickup collision");
                    return;
                }

                // if collision with health
                if (element.type == "health") {
                    element.active = false;
                    this.healthPickup();
                    console.log("health collision");
                    return;
                }

                // invert player velocity
                this.velocity = new Vector2((this.velocity.x * -1) + (element.moveX),
                    (this.velocity.y * -1) + (element.moveY));

                // reduce player health
                this.health--;
                if (this.health < 0) {
                    this.health = 0;
                }
                console.log(this.health);

                // if player slow moving set velocity on collision
                if (this.velocity.x <= 1) {
                    this.velocity.x = (this.velocity.x * 3) + element.moveX * 2;
                }
                if (this.velocity.y <= 1) {
                    this.velocity.y = (this.velocity.y * 3) + element.moveY * 2;
                }

                // if max speed exceeded set velocity to max
                if (this.velocity.magSq() >= this._maxSpeed) {
                    this.velocity.normalize(this._maxSpeed);
                }
                // play audio on collision
                this.collisionAudio.currentTime = 0;
                this.collisionAudio.play();
            }
        });
    }

    // destroy after x ms
    private destroy = (): void => {
        setTimeout(
            function () {
                player.velocity = new Vector2(0, 0);
                player.active = false; // set element active false after x ms 
            }, 1000);
    }

    // speed up
    private accelerate = (): void => {
        if (this.velocity.x == 0 && this.velocity.y == 0) {
            this.velocity.copy(this.orientation);
            this.velocity.multiply(this.acceleration);
        }

        this._tempVec.copy(this.orientation);
        this._tempVec.multiply(this.acceleration);

        this.velocity.add(this._tempVec);
        if (this.velocity.magSq() >= this._maxSpeed) {
            this.velocity.normalize(this._maxSpeed);
        }
    }

    // slow down
    private decelerate = (): void => {
        this.velocity.multiply(0.9);
        if (this.velocity.magSq() < 0.5) {
            this.velocity.x = 0;
            this.velocity.y = 0;
        }
    }

    private turnLeft = (): void => {
        this.rotation -= 0.1;
        this.rotation %= Math.PI * 2;
        this.orientation.x = 1;
        this.orientation.y = 0;
        this.orientation.rotate(-this.rotation);
    }

    private turnRight = (): void => {
        this.rotation += 0.1;
        this.rotation %= Math.PI * 2;
        this.orientation.x = 1;
        this.orientation.y = 0;
        this.orientation.rotate(-this.rotation);
    }

    // shoot a bullet
    private shoot = (): void => {
        let bullet: Bullet;

        if (!this.canShoot) {
            return;
        }

        for (var index = 0; index < bulletArray.length; index++) {
            bullet = bulletArray[index];

            if (!bullet.active) {
                break; // break if bullet active false
            }
        }

        // create new bullet if null or none active
        if (bullet == null || bullet.active) {
            bullet = new Bullet(player.x, player.y, 3);
            bulletArray.push(bullet); // add bullet to array            
        } else { // set bullet positions & active true
            bullet.x = player.x;
            bullet.y = player.y;
            bullet.active = true;
        }
        bullet.move(player.orientation); // start moving bullet from player orientation 

        //play shooting audio
        this.bulletAudio.currentTime = 0; // reset audio on new call
        this.bulletAudio.play();

        // set fire delay
        this.canShoot = false;
        setTimeout(
            function () {
                player.canShoot = true;
            }, 200);
    }

    // on pickup colission
    private pickUp = (type: string): void => {
        if (type == "gold") {
            this.addScore(3);
        }
        if (type == "silver") {
            this.addScore(2);
        }
        if (type == "copper") {
            this.addScore(1);
        }
        // play pickup audio
        this.pickupAudio.currentTime = 0; // reset audio on new call
        this.pickupAudio.play();
    }

    // triggered when health picked up
    private healthPickup = (): void => {
        this.health++;
        if (this.health > this.maxHealth) {
            this.health = this.maxHealth;
        }
        this.healthAudio.currentTime = 0; // reset audio on new call
        this.healthAudio.play();
    }

    // add score
    public addScore = (value: number): void => {
        this._score += value;
    }

    get maxSpeed(): number {
        return this._maxSpeed;
    }
    set maxSpeed(value: number) {
        this._maxSpeed = value;
    }

    get velocity(): Vector2 {
        return this._velocity;
    }
    set velocity(value: Vector2) {
        this._velocity = value;
    }

    get score(): number {
        return this._score;
    }
    set score(value: number) {
        this._score = value;
    }

    get activeEnemies(): number {
        return this._activeEnemies;
    }
    set activeEnemies(value: number) {
        this._activeEnemies = value;
    }

    // trigger on keydown
    private keyboardDown(event: KeyboardEvent) {

        // left arrow or a
        if (event.key == "ArrowLeft" || event.key == "a") {
            player.leftPressed = true;
        }
        // up arrow or w
        if (event.key == "ArrowUp" || event.key == "w") {
            player.upPressed = true;
        }
        // right arrow or d
        if (event.key == "ArrowRight" || event.key == "d") {
            player.rightPressed = true;
        }
        // down arrow or s
        if (event.key == "ArrowDown" || event.key == "s") {
            player.downPressed = true;
        }
        // spacebar
        if (event.keyCode == 32) {
            player.spacePressed = true;
        }
    }

    // reset on keyup
    private keyboardUp(event: KeyboardEvent) {

        // left arrow or a
        if (event.key == "ArrowLeft" || event.key == "a") {
            player.leftPressed = false;
        }
        // up arrow or w
        if (event.key == "ArrowUp" || event.key == "w") {
            player.upPressed = false;
        }
        // right arrow or d
        if (event.key == "ArrowRight" || event.key == "d") {
            player.rightPressed = false;
        }
        // down arrow or s
        if (event.key == "ArrowDown" || event.key == "s") {
            player.downPressed = false;
        }
        // spacebar
        if (event.keyCode == 32) {
            player.spacePressed = false;
        }
    }
}