let canvas: HTMLCanvasElement;
let ctx: CanvasRenderingContext2D;

let objectArray: Array<iObject> = new Array<iObject>();
let bulletArray: Array<Bullet> = new Array<Bullet>();
let bgObjectArray: Array<iBackgroundObject> = new Array<iBackgroundObject>();

let player: Player;

class Game {
    private fps: number = 60; // limit frame update rate

    private uI: UserInterface;
    private timer: number = 0;
    private waveTimer: number = 1000; // time between spawning enemies

    private asteroidAmount: number;
    private circleAmount: number;

    private alarmAudio: HTMLAudioElement = new Audio("./assets/audio/alarm.wav");
    private bgAudio: HTMLAudioElement;

    constructor() {
        console.log("Start");

        // define canvas
        canvas = <HTMLCanvasElement>document.getElementById("canvas");
        ctx = canvas.getContext("2d");

        // set canvas size equal to window size
        ctx.canvas.width = window.innerWidth;
        ctx.canvas.height = window.innerHeight;

        // build interface
        this.uI = new UserInterface();

        // player: x, y, size, color, lineWidth, health
        player = new Player(canvas.width / 2, canvas.height / 2, 5, "white", 5, 3);

        // generate asteroids random position
        this.asteroidAmount = 1000; // set amount of asteroids
        for (let index = 0; index < this.asteroidAmount; index++) {
            var randomX = (Math.random() * 20000) - 10000;
            var randomY = (Math.random() * 20000) - 10000;
            var randomSize = (Math.random() * 500) + 40;

            // move asteroids out of player startposition
            if (randomX >= player.x - 500 && randomX <= player.x + 500 && randomY >= player.y - 500 && randomY <= player.y + 500) {
                randomX = (Math.random() * 1000) - 3000;
                randomY = (Math.random() * 1000) + 3000;
                console.log("Asteroid moved away from spawn");
            }
            objectArray.push(new Asteroid(randomX, randomY, randomSize, randomSize, (randomSize / 5), "./assets/images/asteroid2.png"));
        }

        // Generate random background circles: x, y, radius, color, lineWidth
        this.circleAmount = 150; // set amount of asteroids
        for (let index = 0; index < this.circleAmount; index++) {
            var randomX = (Math.random() * 20000) - 10000;
            var randomY = (Math.random() * 20000) - 10000;

            var randomRadius = (Math.random() * 1000) + 1;
            var randomLineWidth = (Math.random() * 500) + 1;
            var randomColor = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';

            bgObjectArray.push(new Circle(randomX, randomY, randomRadius, randomColor, randomLineWidth));
        }

        // testobjects: x, y, width, height, health/type, source
        objectArray.push(new Pickup(player.x, 500, 60, 60, "health", "./assets/images/heart.png"));
        objectArray.push(new Pickup(player.x + 600, 100, 70, 70, "gold", "./assets/images/gold.png"));

        // select background audio file
        var audioRng = Math.floor(Math.random() * 2) + 1;
        console.log("audiofile: " + audioRng);
        if (audioRng == 1) {
            this.bgAudio = new Audio("./assets/audio/bgAudio.wav");
        } else {
            this.bgAudio = new Audio("./assets/audio/bgAudio2.wav");
        }

        // start & loop background audio
        this.bgAudio.loop = true;
        this.bgAudio.volume = 0.1;
        this.bgAudio.play();

        // start the game loop
        this.gameLoop();
    }

    // runs every update
    private gameLoop = (): void => {

        // request loop
        var Loop = this.gameLoop; // fix for this.gameLoop
        // limit update frequency
        setTimeout(function () {
            requestAnimationFrame(Loop);
        }, 1000 / this.fps);

        this.timer++;
        //console.log(this.timer);

        // spawn enemy every X timer
        if (this.timer % this.waveTimer == 0) {
            this.spawnEnemy();
        }

        // clear canvas before rendering
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // render background objects
        for (var bgObject of bgObjectArray) {
            // move objects based on player movement
            bgObject.x -= (player.velocity.x * 0.5);
            bgObject.y -= (player.velocity.y * 0.5);
            bgObject.render();
        }

        // render iObjects
        for (var object of objectArray) {
            // move objects based on player movement
            object.x -= player.velocity.x;
            object.y -= player.velocity.y;
            object.render();
        }

        // render player
        player.render();
        // set player position to middle of canvas
        player.x = canvas.width / 2;
        player.y = canvas.height / 2;

        // place & render bullets
        for (var bullet of bulletArray) {
            // move bullets based on player movement
            bullet.x -= player.velocity.x;
            bullet.y -= player.velocity.y;
            bullet.render();
        }

        // render UI
        this.uI.render();
    }

    private spawnEnemy = (): void => {
        var maxEnemies = 3;
        var spawnDistance = 600;
        // return if max enemies exceeded or player is dead
        if (player.activeEnemies >= maxEnemies || player.isDestroyed) {
            return;
        }
        var enemySpawn: number = Math.floor(Math.random() * 4) + 1;
        var enemyPosX: number = 0;
        var enemyPosY: number = 0;

        // determine enemy spawn position
        if (enemySpawn == 1) {
            enemyPosX = spawnDistance;
            enemyPosY = spawnDistance;
        }
        if (enemySpawn == 2) {
            enemyPosX = spawnDistance;
            enemyPosY = spawnDistance * -1;
        }
        if (enemySpawn == 3) {
            enemyPosX = spawnDistance * -1;
            enemyPosY = spawnDistance;
        }
        if (enemySpawn == 4) {
            enemyPosX = spawnDistance * -1;
            enemyPosY = spawnDistance * -1;
        }

        var ufoRng: number = Math.floor(Math.random() * 2) + 1;
        objectArray.push(new Enemy(enemyPosX, enemyPosY, 400, 400, 100, "./assets/images/ufo" + ufoRng + ".png"));
        player.activeEnemies++;

        //play alarm audio
        this.alarmAudio.play();
        console.log("enemy spawned " + player.activeEnemies.toString() + " " + enemySpawn.toString());
    }
}

