interface iObject {

    render(): void;
    x: number;
    y: number;
    moveX: number;
    moveY: number;
    
    width: number;
    height: number;     
    active: boolean;  
    isDestroyed: boolean;
    health: number;

    type: string;
    
}
