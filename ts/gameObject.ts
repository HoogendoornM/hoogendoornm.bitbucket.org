class GameObject {

    private _x: number;
    private _y: number;
    private _width: number;
    private _height: number;
    
    private _health: number = 10;

    private _type: string;

    moveX: number;
    moveY: number;
    active: boolean = true;
    isDestroyed: boolean = false;
    radius: number = 10;
    lineWidth: number = 2;
    color: string = "red";

    get x(): number {
        return this._x;
    }
    set x(value: number) {
        this._x = value;
    }

    get y(): number {
        return this._y;
    }
    set y(value: number) {
        this._y = value;
    }

    get width(): number {
        return this._width;
    }
    set width(value: number) {
        this._width = value;
    }

    get height(): number {
        return this._height;
    }
    set height(value: number) {
        this._height = value;
    }

    get health(): number {
        return this._health;
    }
    set health(value: number) {
        this._health = value;
    }

    get type(): string {
        return this._type;
    }
    set type(value: string) {
        this._type = value;
    }
}
