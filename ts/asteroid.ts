/// <reference path="gameObject.ts" />
class Asteroid extends GameObject implements iObject {

    private img: HTMLImageElement = new Image;
    private explosionImg: HTMLImageElement = new Image;

    private startHealth: number; 

    private explosionAudio: HTMLAudioElement = new Audio("./assets/audio/explosion.wav");

    constructor(x: number, y: number, width: number, height: number, health: number, src: string) {
        super();
        this.x = x;
        this.y = y;
        this.width = width
        this.height = height
        this.health = health;
        this.startHealth = health;

        this.img.src = src;
        this.img.width = this.width;
        this.img.height = this.height;

        this.explosionImg.src = "./assets/images/explosion.png";
        this.explosionImg.width = this.width;
        this.explosionImg.height = this.height;

        // add random movement
        this.moveX = (Math.random() * 2) - 1;
        this.moveY = (Math.random() * 2) - 1;        
    }

    // render asteroid
    // runs every frame
    public render = (): void => {

        // if no more health destroy
        if (this.health <= 0) {
            if (!this.isDestroyed) {
                this.destroy();
            }
            this.isDestroyed = true; // changes image to explosion            
        }
        // do nothing if not active
        if (!this.active) {
            return;
        }        
        
        ctx.save();        
        
        this.x += this.moveX;
        this.y += this.moveY;       

        // change render image if destroyed
        if (!this.isDestroyed) {
            ctx.drawImage(this.img, this.x, this.y, this.width, this.height);
        } else {
            ctx.drawImage(this.explosionImg, this.x, this.y, this.width, this.height);
        }
        
        ctx.restore();
    }
    // destroy after x ms  
    public destroy = (): void => {
        let temp = this; // fix for this.active
        // create smaller asteroids or pickup
        if (temp.width > 150) {
            var randomX = (Math.random() * 100) - 50;
            var randomY = (Math.random() * 100) - 50;            
            var asteroid1 = new Asteroid((temp.x + temp.width /4) + randomX, (temp.y + (temp.height /4)) + randomY, (temp.width / 2), (temp.height / 2), 30, "./assets/images/asteroid2.png");
            var asteroid2 = new Asteroid((temp.x + temp.width /4) + (randomX * -1), (temp.y + (temp.height /4)) + (randomY * -1), (temp.width / 2), (temp.height / 2), 30, "./assets/images/asteroid2.png");
            objectArray.push(asteroid1);
            objectArray.push(asteroid2);
        } else {
            // spawn pickup gold, silver or copper
            var randomType: string;
            var typeRng: number = Math.floor(Math.random() * 3) + 1;
            console.log(typeRng);
            
            if (typeRng == 1){
                randomType = "gold";
            }
            if (typeRng == 2){
                randomType = "silver";
            }
            if (typeRng == 3){
                randomType = "copper";
            }

            var pickup1 = new Pickup(temp.x, temp.y, 70, 70, randomType, "./assets/images/"+randomType+".png");
            objectArray.push(pickup1);
            console.log(randomType + " spawned");
        }

        // play sound on death
        this.explosionAudio.currentTime = 0; // reset audio on new call
        this.explosionAudio.play();

        // remove explosion after x ms
        setTimeout(
            function () {
                temp.active = false; // set element active false after x ms 
            }, 150);
    }
}