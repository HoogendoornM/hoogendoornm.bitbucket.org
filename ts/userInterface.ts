class UserInterface {

    private heartImg: HTMLImageElement = new Image;
    private deadAudio: HTMLAudioElement = new Audio("./assets/audio/dead.wav");

    private playedAudio: boolean = false; 

    // constructor
    constructor() {
        this.heartImg.src = "./assets/images/heart.png";
    }

    // Render the object
    // runs every gameloop
    public render = (): void => {

        // startcolor
        ctx.fillStyle = "white";

        // render player score
        ctx.font = "35px Verdana";
        ctx.fillStyle = "white";
        ctx.fillText("Score: " + player.score.toString(), canvas.width - 200, 50);

        // render player health
        var heartPos: number = 50;
        for (let index = 0; index < player.health; index++) {
            ctx.drawImage(this.heartImg, heartPos, 10, 50, 50);
            heartPos += 40;
        }

        // render endscreen if player health 0
        if (player.health <= 0) {

            // play audio            
            if (!this.playedAudio){          
            this.deadAudio.play();
            this.playedAudio = true;
            }

            // render YOU DIED
            ctx.font = "50px Verdana";
            ctx.fillStyle = "red";
            ctx.fillText("YOU DIED", (canvas.width / 2) - 100, canvas.height / 3);

            // render final score
            ctx.fillStyle = "white";
            ctx.fillText("Final score: " + player.score.toString(), (canvas.width / 2) - 150, canvas.height / 2);

            // render F5 to restart
            ctx.fillStyle = "white";
            ctx.font = "35px Verdana";
            ctx.fillText("Press F5 to restart!", (canvas.width / 2) - 150, canvas.height / 1.5);

        }
    }
}