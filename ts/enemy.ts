/// <reference path="gameObject.ts" />
class Enemy extends GameObject implements iObject {

    private img = new Image;
    private explosionImg = new Image;

    private startHealth: number; 
    private speed: number = 1;
    private rotation: number = 0;

    private explosionAudio: HTMLAudioElement = new Audio("./assets/audio/explosion.wav");

    constructor(x: number, y: number, width: number, height: number, health: number, src: string) {
        super();
        this.x = x;
        this.y = y;
        this.width = width
        this.height = height
        this.health = health;
        this.startHealth = health;

        this.img.src = src;
        this.img.width = this.width;
        this.img.height = this.height;

        this.explosionImg.src = "./assets/images/explosion.png";
        this.explosionImg.width = this.width;
        this.explosionImg.height = this.height;
    }

    // render enemy
    // runs every frame
    public render = (): void => {

        // if no more health destroy
        if (this.health <= 0) {
            if (!this.isDestroyed) {
                this.destroy();
            }
            this.isDestroyed = true; // changes image to explosion            
        }
        // do nothing if not active
        if (!this.active) {
            return;
        }     
        
        // movement target player position
        // calculate X movement
        if (this.x + (this.width / 2) < player.x) {
            this.moveX = this.speed;
        } else {
            this.moveX = -this.speed;
        }
        // calculate Y movement
        if (this.y + (this.height / 2) < player.y) {
            this.moveY = this.speed;
        } else {
            this.moveY = -this.speed;
        }        

        this.x += this.moveX;
        this.y += this.moveY;
        
        ctx.save();

        // change render image if destroyed
        if (!this.isDestroyed) {
            ctx.drawImage(this.img, this.x, this.y, this.width, this.height);
        } else {
            ctx.drawImage(this.explosionImg, this.x, this.y, this.width, this.height);
        }
        
        ctx.restore();
    }
    // destroy after x ms  
    public destroy = (): void => {
        let temp = this; // fix for this.active
        player.activeEnemies--;

        // spawn health on death
        var healthPickup = new Pickup((temp.x + temp.width /2), (temp.y + temp.height /2), 70, 70, "health", "./assets/images/heart.png");
            objectArray.push(healthPickup);
            console.log("Health spawned");

        // play sound on death
        this.explosionAudio.currentTime = 0; // reset audio on new call
        this.explosionAudio.play();

        // remove explosion after x ms
        setTimeout(
            function () {
                temp.active = false; // set element active false after x ms 
            }, 150);
    }
}